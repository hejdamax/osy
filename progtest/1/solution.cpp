#ifndef __PROGTEST__
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <climits>
#include <cfloat>
#include <cassert>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <numeric>
#include <vector>
#include <set>
#include <list>
#include <map>
#include <unordered_set>
#include <unordered_map>
#include <queue>
#include <stack>
#include <deque>
#include <memory>
#include <functional>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include "progtest_solver.h"
#include "sample_tester.h"
using namespace std;
#endif /* __PROGTEST__ */
class CSentinelHacker
{
  public:
    static bool SeqSolve(const vector<uint64_t> &fragments, CBigInt &res);
    void AddTransmitter(ATransmitter x);
    void AddReceiver(AReceiver x);
    void AddFragment(uint64_t x);
    void Start(unsigned thrCount);
    void Stop(void);
    void worker(unsigned int threadID);
    void transmitter(unsigned int threadID);
    void reciever(unsigned int threadID);
    pair<u_int32_t, vector<u_int64_t>> mapInsert(u_int64_t & fragment);
    CBigInt getSolution(pair<u_int32_t, vector<u_int64_t>> & message);
    void writeToOutput();
  private:
    size_t recv = 0;
    size_t tran = 0;
    map<u_int32_t, vector<uint64_t>> messages;
    vector<thread> workerPool;
    vector<thread> recieverPool;
    vector<thread> transmitterPool;
    vector<ATransmitter> transmitters;
    vector<AReceiver> recievers;
    queue<u_int64_t> input;
    queue<pair<u_int32_t, pair<CBigInt, bool>>> output;
    mutex inputQueueLock;
    // mutex recieverMTX;
    // mutex AddFragmentMTX;
    mutex maplock;
    mutex outputQueueLock;
    mutex transmitterMTX;
    mutex threadCounter;
    condition_variable inputQueueCondVar;
    condition_variable outputQueueCondVar;
    size_t ThreadCount = 0;
    bool toEnd = false;
    bool toEndTransimtter = false;

};
void toLog(string operation, thread::id threadID)
{
  cout << "Thread: " << threadID << "\n\t" << operation << endl;
}
int cmp(CBigInt lhs, CBigInt rhs)
{
  return !lhs.CompareTo(rhs);
}

u_int32_t getID(u_int64_t number)
{
  u_int32_t half = number >> 37;
  return half & 0x7FFFFFF;
}

// TODO: CSentinelHacker implementation goes here
//-------------------------------------------------------------------------------------------------

void CSentinelHacker::worker(unsigned int threadID)
{
  while (true)
  {
    //input fragments into map and try to put together...
    //when stop is called empty input queue and push whole map into output
    unique_lock<mutex> inputQueueLck(inputQueueLock);
    //waiting for something in queue
    toLog("Waiting for data", this_thread::get_id());
    inputQueueCondVar.wait(inputQueueLck, [&](){/*cout << "New job" << endl;*/ return input.size() > 0 || toEnd;});

    //one message from map (all recieved fragments and ID)
    pair<u_int32_t, vector<u_int64_t>> message;
    if(input.size() > 0)
    {
      //inserting into map
      toLog("Input into map", this_thread::get_id());
      message = mapInsert(input.front());
      input.pop();
      inputQueueLck.unlock();  
    }

    //get solution
    toLog("Trying to solve", this_thread::get_id());
    unique_lock<mutex> thr(threadCounter);
    ThreadCount++;
    thr.unlock();
    CBigInt res =  getSolution(message);
    
    if(!res.IsZero())
    {
      toLog("Solved " + to_string(message.first), this_thread::get_id());
      unique_lock<mutex> sendComplete(outputQueueLock);
      output.push({message.first, {res, false}});
      outputQueueCondVar.notify_one();
      sendComplete.unlock();
    }
    unique_lock<mutex> thr1(threadCounter);
    ThreadCount--;
    thr1.unlock();
    //stop was called: clear out map and insert everything into output queue
    if(toEnd && input.empty() && ThreadCount == 0)
    { 
      unique_lock<mutex> finish(outputQueueLock);
      writeToOutput();
      finish.unlock();
      break;
    }
   
  }
  
}

pair<u_int32_t, vector<u_int64_t>> CSentinelHacker::mapInsert(u_int64_t & fragment)
{
  u_int32_t id = getID(input.front());
  auto msg = messages.find(id);
      // check if key exists
  if(msg == messages.end())
  {
    //getting iterator to key
    msg = messages.insert({id, vector<u_int64_t>()}).first;
  }
  msg -> second.push_back(input.front());
  return make_pair(id, msg -> second);
}

CBigInt CSentinelHacker::getSolution(pair<u_int32_t, vector<u_int64_t>> & message)
{
  CBigInt res;
  if(SeqSolve(message.second, res))
  {
    unique_lock<mutex> mapDelete(maplock);
    toLog("Delete from map" + to_string(message.first), this_thread::get_id());
    messages.erase(message.first);
    mapDelete.unlock();
  }
  return res;
}

void CSentinelHacker::writeToOutput()
{
  toLog("Ending: writing everything into output Queue ", this_thread::get_id());
  for(auto it = messages.begin(); it != messages.end();)
  {
    CBigInt res = 0;
    
    if(!SeqSolve(it -> second, res))
    {
      toLog("Pushing Incomplete message " + to_string(it -> first), this_thread::get_id());
      output.push({it -> first, {res, true}});
    }
    else
    {
      toLog("Pushing Complete message " + to_string(it -> first), this_thread::get_id());
      output.push({it -> first, {res, false}});
    }
    toLog("Delete from map end Variant" + to_string(it -> first), this_thread::get_id());
    it = messages.erase(it);
    outputQueueCondVar.notify_all();
  }
}

void CSentinelHacker::transmitter(unsigned int threadID)
{
  while (true)
  {
    //lock the outout queue
    unique_lock<mutex>  TransmitterLock(outputQueueLock);
    toLog("Waiting to transmitt", this_thread::get_id());
    //wait for workers to push into this queue
    outputQueueCondVar.wait(TransmitterLock, [&](){/*cout << "Send Out" << endl*/; return output.size() > 0 || toEndTransimtter;});

    //cycle through the transmitters and send out the first thing in queue
    for(auto it = transmitters.begin(); it != transmitters.end(); it++)
    {
      toLog("Transmitting " + to_string(output.front().first), this_thread::get_id());
      if(output.size() > 0)
      {
        //if is incomplete
        if(output.front().second.second)
        {
          it -> get() -> Incomplete(output.front().first);
        }
        else //is complete
        {
          it -> get() -> Send(output.front().first, output.front().second.first);
        }
        //remove the item from the queue;
        toLog("Removing from output " + to_string(output.front().first), this_thread::get_id());
        output.pop();
      }
    }
    //if the queue is empty and stop was called break out
    if(toEndTransimtter && output.empty())
    {
      toLog("Ending transmitters", this_thread::get_id());
      break;
    }
    TransmitterLock.unlock();
  }
  
}

void CSentinelHacker::reciever(unsigned int threadID)
{
  //while recievers have data cycle through them and extract it;
  //then break and wait for stop...
  bool empty = false;
  while(!empty)
  {
    toLog("Recieving Reciever", this_thread::get_id());
    for(auto it = recievers.begin(); it != recievers.end(); it++)
    {
      u_int64_t frag = 0;
      if((it) -> get() -> Recv(frag))
      {
        //locking the input queue and pushing the fragment into it
        unique_lock<mutex> lock(inputQueueLock);
        input.push(frag);
        inputQueueCondVar.notify_one();
        lock.unlock();
      }
     else
     {
       toLog("Ending reciever", this_thread::get_id());
       empty = true;
       break;
     }
     
    }
    
  }
}

bool CSentinelHacker::SeqSolve(const vector<uint64_t> &fragments, CBigInt &res)
{
  if( FindPermutations(fragments.data(), fragments.size(), [&res](const uint8_t* data, size_t bits)
  {CBigInt rs = CountExpressions(data + 4, bits - 32); res = max(rs, res, cmp);}) == 0)
  {
    return false;
  }
  return true;
}

void CSentinelHacker::AddTransmitter(ATransmitter x)
{
  transmitters.push_back(x);
}

void CSentinelHacker::AddReceiver(AReceiver x)
{
  recievers.push_back(x);
}

void CSentinelHacker::AddFragment(uint64_t x)
{
  toLog("Recieving Add Fragment", this_thread::get_id());
  unique_lock<mutex> lock(inputQueueLock);
  input.push(x);
  inputQueueCondVar.notify_one();
  lock.unlock();
}

void CSentinelHacker::Start(unsigned thrCount)
{
  toLog("\n\e[4mStart was called\e[0m\n", this_thread::get_id());
  //starting the worker threads
  for(size_t i = 0; i < thrCount; i++)
  {
    workerPool.push_back(thread(&CSentinelHacker::worker, this, i));
  }
  //starting the threads that service the transmittters
  for(size_t i = 0; i < transmitters.size(); i++)
  {
    transmitterPool.push_back(thread(&CSentinelHacker::transmitter, this, i));
  }
  //starting the threads that service the recievers
  for(size_t i = 0; i < recievers.size(); i++)
  {
    recieverPool.push_back(thread(&CSentinelHacker::reciever, this, i));
  }
}

void CSentinelHacker::Stop(void)
{
  //join all recievers;
  toLog("\n\e[4mStop was called\e[0m\n", this_thread::get_id());
  for(auto it = recieverPool.begin(); it != recieverPool.end();)
  {
    toLog("\n\tJoining reciever threads", this_thread::get_id());
    (*it).join();
    it = recieverPool.erase(it);
    toLog("\n\tJoined reciever threads", this_thread::get_id());
  }
  //notify all worker threads that the program should end;
  toLog("Notifying all workers that stop was called", this_thread::get_id());
  toEnd = true;
  inputQueueCondVar.notify_all();
  //join all worker threads;
  for(auto it = workerPool.begin(); it != workerPool.end();)
  {
    toLog("\n\tJoining worker threads", this_thread::get_id());
    (*it).join();
    it = workerPool.erase(it);
    toLog("\n\tJoined worker threads", this_thread::get_id());
  }
  //join all transmitter threads;
  toEndTransimtter = true;
  outputQueueCondVar.notify_all();
  for(auto it = transmitterPool.begin(); it != transmitterPool.end();)
  {
    toLog("\n\tJoining transmitter threads", this_thread::get_id());
    (*it).join();
    it = transmitterPool.erase(it);
    toLog("\tJoined transmitter threads", this_thread::get_id());
  }
}

#ifndef __PROGTEST__
int main(void)
{
  using namespace std::placeholders;
  for (const auto &x : g_TestSets)
  {
    CBigInt res;
    assert(CSentinelHacker::SeqSolve(x.m_Fragments, res));
    assert(CBigInt(x.m_Result).CompareTo(res) == 0);
  }

  for(int i = 0; i < 100; i++)
  {
    CSentinelHacker test;
    auto trans = make_shared<CExampleTransmitter>();
    AReceiver recv = make_shared<CExampleReceiver>(initializer_list<uint64_t>{0x02230000000c, 0x071e124dabef, 0x02360037680e, 0x071d2f8fe0a1, 0x055500150755});

    test.AddTransmitter(trans);
    test.AddReceiver(recv);
    
    
    test.Start(5);

    static initializer_list<uint64_t> t1Data = {0x071f6b8342ab, 0x0738011f538d, 0x0732000129c3, 0x055e6ecfa0f9, 0x02ffaa027451, 0x02280000010b, 0x02fb0b88bc3e};
    thread t1(FragmentSender, bind(&CSentinelHacker::AddFragment, &test, _1), t1Data);

    static initializer_list<uint64_t> t2Data = {0x073700609bbd, 0x055901d61e7b, 0x022a0000032b, 0x016f0000edfb};
    thread t2(FragmentSender, bind(&CSentinelHacker::AddFragment, &test, _1), t2Data);
    FragmentSender(bind(&CSentinelHacker::AddFragment, &test, _1), initializer_list<uint64_t>{0x017f4cb42a68, 0x02260000000d, 0x072500000025});
    t1.join();
    t2.join();
    test.Stop();
    assert(trans->TotalSent() == 4);
    assert(trans->TotalIncomplete() == 2);
  }
  return 0;
}
#endif /* __PROGTEST__ */
